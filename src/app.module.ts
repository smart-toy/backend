import { Module } from '@nestjs/common'
import { APP_FILTER } from '@nestjs/core'
import { PrismaKnownExceptionFilter } from './shared/filters/prisma-known-exception.filter'
import { PrismaModule } from './prisma/prisma.module'
import { UserModule } from './modules/user/user.module'
import { AuthModule } from './modules/auth/auth.module'
import { appConfigBuilder } from '@/config/config'
import { ConfigModule } from '@nestjs/config'
import { GraphQLModule } from '@nestjs/graphql'
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo'
import { GqlConfigService } from '@/gql-config.service'
import { OrganizationModule } from '@/modules/organization/organization.module'
import { LessonModule } from '@/modules/lesson/lesson.module'
import { AdminModule } from '@/modules/admin/admin.module'

// const DEFAULT_ADMIN = {
//     email: 'admin@example.com',
//     password: 'password',
// }
//
// const authenticate = async (email: string, password: string) => {
//     if (email === DEFAULT_ADMIN.email && password === DEFAULT_ADMIN.password) {
//         return Promise.resolve(DEFAULT_ADMIN)
//     }
//     return null
// }
//
// void import('adminjs').then((adminjs) => {
//     void import('@adminjs/prisma').then((AdminJSPrisma) => {
//         adminjs.default.registerAdapter({
//             Resource: AdminJSPrisma.Resource,
//             Database: AdminJSPrisma.Database,
//         })
//     })
// })

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true, load: [appConfigBuilder] }),
        // ThrottlerModule.forRoot({ ttl: 60, limit: 60 }),
        PrismaModule.forRoot({
            isGlobal: true,
            prismaServiceOptions: {
                prismaOptions: {
                    log: ['warn', 'error'],
                },
                explicitConnect: false,
            },
        }),
        GraphQLModule.forRootAsync<ApolloDriverConfig>({
            driver: ApolloDriver,
            useClass: GqlConfigService,
        }),

        AdminModule,

        // AdminJS version 7 is ESM-only. In order to import it, you have to use dynamic imports.
        // import('@adminjs/nestjs').then(({ AdminModule }) => AdminModule.createAdminAsync({
        //     useFactory: () => ({
        //         adminJsOptions: {
        //             rootPath: '/admin',
        //             resources: [],
        //         },
        //         auth: {
        //             authenticate,
        //             cookieName: 'adminjs',
        //             cookiePassword: 'secret',
        //         },
        //         sessionOptions: {
        //             resave: true,
        //             saveUninitialized: true,
        //             secret: 'secret',
        //         },
        //     }),
        // })),

        // Modules w/o imports
        UserModule,
        OrganizationModule,
        LessonModule,
        // Modules w/ imports and exports
        // Modules w/ imports and w/o exports
        AuthModule,
    ],
    controllers: [],
    providers: [
        // {
        //     provide: APP_GUARD,
        //     useClass: ThrottlerBehindProxyGuard,
        // },
        {
            provide: APP_FILTER,
            useClass: PrismaKnownExceptionFilter,
        },
    ],
})
export class AppModule {
}
