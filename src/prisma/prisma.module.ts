import { PrismaService } from './prisma.service'
import { DynamicModule, Module } from '@nestjs/common'
import { PRISMA_SERVICE_OPTIONS } from './prisma.constants'
import { PrismaModuleOptions } from './prisma.interface'


@Module({
    providers: [PrismaService],
    exports: [PrismaService],
})
export class PrismaModule {

    static forRoot(options: PrismaModuleOptions = {}): DynamicModule {
        return {
            global: options.isGlobal,
            module: PrismaModule,
            providers: [
                {
                    provide: PRISMA_SERVICE_OPTIONS,
                    useValue: options.prismaServiceOptions,
                },
            ],
        }
    }

}
