import 'reflect-metadata'
import { HttpAdapterHost, NestFactory, Reflector } from '@nestjs/core'
import { AppModule } from './app.module'
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common'
import { NestExpressApplication } from '@nestjs/platform-express'
import { PrismaKnownExceptionFilter } from './shared/filters/prisma-known-exception.filter'
import { ErrorExceptionFilter } from './shared/filters/error-exception.filter'
import { mw } from 'request-ip'
import { swaggerDocument, swaggerDocumentOptions, swaggerPath, swaggerSetupOptions } from './config/swagger'
import { SwaggerModule } from '@nestjs/swagger'
import { sortObject } from './shared/utils-function'
import { join } from 'path'
import { ConfigService } from '@nestjs/config'
import { AssetsConfig, CorsConfig, NestConfig, SwaggerConfig } from '@/config/config.interface'
import { appConfig } from '@/config/config'


async function bootstrap(): Promise<void> {

    const app = await NestFactory.create<NestExpressApplication>(AppModule)

    const configService = app.get(ConfigService)
    const nestConfig = configService.get<NestConfig>('nest')
    const corsConfig = configService.get<CorsConfig>('cors')
    const swaggerConfig = configService.get<SwaggerConfig>('swagger')
    const assetsConfig = configService.get<AssetsConfig>('assets')

    // -----------------------------------
    // STATIC ASSETS
    // -----------------------------------
    app.useStaticAssets(
        join(process.cwd(), appConfig.assets.dir),
        {
            index: false,
            prefix: '/public/files',
        },
    )

    // -----------------------------------
    // FILTERS
    // -----------------------------------
    const { httpAdapter } = app.get(HttpAdapterHost)
    app.useGlobalFilters(
        new ErrorExceptionFilter(httpAdapter),
        new PrismaKnownExceptionFilter(httpAdapter),
    )

    // -----------------------------------
    // INTERCEPTORS
    // -----------------------------------
    app.useGlobalInterceptors(
        new ClassSerializerInterceptor(app.get(Reflector), {
            exposeUnsetFields: true,
            excludeExtraneousValues: false,
        }),
    )

    // -----------------------------------
    // VALIDATION
    // -----------------------------------
    app.useGlobalPipes(
        new ValidationPipe({
            transform: true,
            disableErrorMessages: false,
            transformOptions: {
                excludeExtraneousValues: false,
                enableImplicitConversion: false,
            },
        }),
    )
    app.use(mw())

    // -----------------------------------
    // SWAGGER
    // -----------------------------------
    if (swaggerConfig?.enabled) {
        const document = SwaggerModule.createDocument(app, swaggerDocument, swaggerDocumentOptions)
        if (document.components?.schemas) {
            document.components.schemas = sortObject(document.components.schemas)
        }
        SwaggerModule.setup(swaggerPath, app, document, swaggerSetupOptions)
    }

    // -----------------------------------
    // CORS
    // -----------------------------------
    if (corsConfig?.enabled) {
        app.enableCors({
            origin: true,
            methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
            credentials: true,
        })
    }

    // -----------------------------------
    // SHUTDOWN HOOKS
    // -----------------------------------
    app.enableShutdownHooks()

    // -----------------------------------
    // START SERVER
    // -----------------------------------
    await app.listen(appConfig.nest.port)
    console.log(`Application is running on: ${await app.getUrl()}`)
}

void bootstrap()
