export interface Config {
    nest: NestConfig
    cors: CorsConfig
    swagger: SwaggerConfig
    graphql: GraphqlConfig
    security: SecurityConfig
    assets: AssetsConfig
}


export interface NestConfig {
    port: number
}


export interface CorsConfig {
    enabled: boolean
}


export interface SwaggerConfig {
    enabled: boolean
}


export interface GraphqlConfig {
    sandboxEnabled: boolean
    debug: boolean
    schemaDestination: string
    sortSchema: boolean
}


export interface SecurityConfig {
    accessKey: string
    refreshKey: string
    expiresIn: string
    refreshIn: string
    bcryptRounds: number
}


export interface AssetsConfig {
    dir: string
    maxSize: number
}
