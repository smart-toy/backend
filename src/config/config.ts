import { Config } from './config.interface'
import * as dotenv from 'dotenv'

// TODO || Use NestJS for read .env
// TODO || Можно ли использовать ConfigService без undefined с default values
dotenv.config()

const appConfig: Config = {
    nest: {
        port: Number(process.env['PORT']) || 3000,
    },
    cors: {
        enabled: true,
    },
    swagger: {
        enabled: true,
    },
    graphql: {
        sandboxEnabled: true,
        debug: true,
        schemaDestination: './src/schema.graphql',
        sortSchema: true,
    },
    security: {
        accessKey: process.env['JWT_ACCESS_SECRET']!,
        refreshKey: process.env['JWT_REFRESH_SECRET']!,
        expiresIn: '10d',
        refreshIn: '20d',
        bcryptRounds: 10,
    },
    assets: {
        dir: 'public/files',
        maxSize: 10 * 1024 * 1024,  // 50 MB
    },
}

const appConfigBuilder = (): Config => appConfig

export { appConfig, appConfigBuilder }
