import { BadRequestException, Injectable } from '@nestjs/common'
import { JwtPayload } from './dto/jwt-payload'
import { JwtService } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'
import { SecurityConfig } from '@/config/config.interface'
import { Token } from '@/modules/auth/models/token.model'


@Injectable()
export class TokenService {

    securityConfig?: SecurityConfig

    constructor(
        private readonly jwtService: JwtService,
        configService: ConfigService,
    ) {
        this.securityConfig = configService.get<SecurityConfig>('security')
    }

    generateTokens({ userId }: JwtPayload): Token {
        const accessToken = this.generateAccessToken({ userId })
        const refreshToken = this.generateRefreshToken({ userId })

        return { accessToken, refreshToken }
    }

    refreshToken(refreshToken: string): Token {
        const refreshSecret = this.securityConfig?.refreshKey

        try {
            const payload = this.jwtService.verify(refreshToken, { secret: refreshSecret })
            return this.generateTokens(payload)
        } catch (err: unknown) {
            throw new BadRequestException('Invalid refresh token')
        }
    }

    private generateAccessToken(payload: JwtPayload): string {
        return this.jwtService.sign(payload)
    }

    private generateRefreshToken(payload: JwtPayload): string {
        return this.jwtService.sign(payload, {
            secret: this.securityConfig?.refreshKey,
            expiresIn: this.securityConfig?.refreshIn,
        })
    }
}
