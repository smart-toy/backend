import { Args, Mutation, Parent, ResolveField, Resolver } from '@nestjs/graphql'
import { AuthService } from './auth.service'
import { Auth } from './models/auth.model'
import { Token } from './models/token.model'
import { LoginInput } from '@/modules/auth/dto/login.dto'
import { RefreshTokenInput } from '@/modules/auth/dto/refresh.dto'
import { User } from '@generated/user/user.model'
import { TokenService } from '@/modules/auth/token.service'


@Resolver(() => Auth)
export class AuthResolver {
    constructor(private readonly authService: AuthService, private readonly tokenService: TokenService) {
    }

    @Mutation(() => Auth)
    async login(@Args('data') { email, password }: LoginInput) {
        const { accessToken, refreshToken } = await this.authService.login(
            email,
            password,
        )

        return {
            accessToken,
            refreshToken,
        }
    }

    @Mutation(() => Token)
    async refreshToken(@Args() { token }: RefreshTokenInput) {
        return this.tokenService.refreshToken(token)
    }

    @ResolveField('user', () => User)
    async user(@Parent() auth: Auth) {
        return await this.authService.getUserFromToken(auth.accessToken)
    }
}
