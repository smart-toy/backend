import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { AuthService } from './auth.service'
import { JwtPayload } from './dto/jwt-payload'
import { ConfigService } from '@nestjs/config'
import { SecurityConfig } from '@/config/config.interface'
import { User } from '@generated/user/user.model'


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(
        private readonly authService: AuthService,
        private readonly configService: ConfigService,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            // TODO || Включить
            ignoreExpiration: true,
            secretOrKey: configService.get<SecurityConfig>('security')?.accessKey,
        })
    }

    async validate(payload: JwtPayload): Promise<User> {
        const user = await this.authService.validateUser(payload.userId)
        if (!user) {
            throw new UnauthorizedException()
        }
        return user
    }

}
