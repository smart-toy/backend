import { Module } from '@nestjs/common'
import { AuthService } from './auth.service'
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { JwtStrategy } from './jwt.strategy'
import { PasswordService } from './password.service'
import { TokenService } from './token.service'
import { ConfigService } from '@nestjs/config'
import { SecurityConfig } from '@/config/config.interface'
import { AuthResolver } from '@/modules/auth/auth.resolver'
import { GqlAuthGuard } from '@/modules/auth/gql-auth.guard'


@Module({
    imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => {
                const securityConfig = configService.get<SecurityConfig>('security')
                return {
                    secret: securityConfig?.accessKey,
                    signOptions: {
                        expiresIn: securityConfig?.expiresIn ?? '5m',
                    },
                }
            },
            inject: [ConfigService],
        }),
    ],
    providers: [
        AuthService,
        TokenService,
        AuthResolver,
        JwtStrategy,
        GqlAuthGuard,
        PasswordService,
    ],
    exports: [GqlAuthGuard],
})
export class AuthModule {
}
