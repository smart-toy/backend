import { Injectable } from '@nestjs/common'
import { compare as bCompare, hash as bHash } from 'bcrypt'
import { ConfigService } from '@nestjs/config'
import { SecurityConfig } from '@/config/config.interface'


@Injectable()
export class PasswordService {

    private readonly bcryptRounds: number

    constructor(configService: ConfigService) {
        this.bcryptRounds = configService.get<SecurityConfig>('security')?.bcryptRounds ?? 10
    }

    async hash(password: string): Promise<string> {
        return bHash(password, this.bcryptRounds)
    }

    async compare(password: string, hashedPassword: string): Promise<boolean> {
        return bCompare(password, hashedPassword)
    }
}
