import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PrismaService } from '@/prisma/prisma.service'
import { PasswordService } from './password.service'
import { TokenService } from './token.service'
import { JwtService } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'
import { Token } from '@/modules/auth/models/token.model'
import { User } from '@generated/user/user.model'


@Injectable()
export class AuthService {

    constructor(
        private readonly jwtService: JwtService,
        private readonly prisma: PrismaService,
        private readonly passwordService: PasswordService,
        private readonly configService: ConfigService,
        private readonly tokenService: TokenService,
    ) {
    }

    async login(email: string, password: string): Promise<Token> {
        const user = await this.prisma.user.findUniqueOrThrow({
            where: { email: email.toLowerCase() },
        })

        if (!(await this.passwordService.compare(password, user.password))) {
            throw new UnauthorizedException('Invalid credentials')
        }

        return this.tokenService.generateTokens({ userId: user.id })
    }

    async validateUser(id: string): Promise<User | null> {
        return this.prisma.user.findUnique({ where: { id } })
    }

    async getUserFromToken(token: string): Promise<User | null> {
        const id: string | null = await this.jwtService.decode(token)?.['userId'] ?? null
        if (!id) {
            return null
        }
        return this.prisma.user.findUnique({ where: { id } })
    }
}
