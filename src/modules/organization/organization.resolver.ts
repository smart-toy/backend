import { Args, Query, Resolver } from '@nestjs/graphql'
import { OrganizationService } from './organization.service'
import { Organization } from '@generated/organization/organization.model'


@Resolver(() => Organization)
export class OrganizationResolver {
    constructor(
        private readonly organizationService: OrganizationService,
    ) {
    }

    @Query(() => [Organization])
    async organizations(): Promise<Organization[]> {
        return this.organizationService.findAll()
    }

    @Query(() => Organization)
    async organization(@Args('id', { type: () => String }) id: string): Promise<Organization> {
        return this.organizationService.findOne(id)
    }
}
