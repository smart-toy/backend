import { Injectable } from '@nestjs/common'
import { PrismaService } from '@/prisma/prisma.service'
import { Organization } from '@generated/organization/organization.model'


@Injectable()
export class OrganizationService {
    constructor(
        private readonly prisma: PrismaService,
    ) {
    }

    async findAll(): Promise<Organization[]> {
        return this.prisma.organization.findMany()
    }

    async findOne(id: string): Promise<Organization> {
        // TODO ||
        return this.prisma.user.findUniqueOrThrow({ where: { id } })
    }
}
