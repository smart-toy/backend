import { PrismaClient } from '@prisma/client'
import { hash } from 'bcrypt'


const DEFAULT_ADMIN = {
    email: 'admin@admin.ru',
    password: 'admin',
}

const authenticate = async (email: string, password: string) => {
    if (email === DEFAULT_ADMIN.email && password === DEFAULT_ADMIN.password) {
        return Promise.resolve(DEFAULT_ADMIN)
    }
    return null
}

const bHash = async (pass: string) => hash(pass, 13)

const localProvider = {
    bucket: 'public/files',
    opts: {
        baseUrl: '/files',
    },
}

export const AdminModule = Promise.all([
    import('@adminjs/nestjs'),
    import('@adminjs/prisma'),
    import('adminjs'),
    import('@adminjs/passwords'),
    import('@adminjs/upload'),
]).then(
    ([
         { AdminModule },
         { Database, Resource, getModelByName },
         { default: AdminJS, ComponentLoader },
         { default: passwordsFeature },
         { default: uploadFeature },
     ]) => {

        AdminJS.registerAdapter({ Resource, Database })

        const componentLoader = new ComponentLoader()
        const prisma = new PrismaClient()

        return AdminModule.createAdminAsync({
            useFactory: () => ({
                adminJsOptions: {
                    rootPath: '/admin',
                    resources: [
                        { resource: { model: getModelByName('Organization'), client: prisma } },
                        { resource: { model: getModelByName('LessonStructure'), client: prisma } },
                        {
                            resource: { model: getModelByName('Lesson'), client: prisma },
                            features: [
                                uploadFeature({
                                    componentLoader,
                                    provider: { local: localProvider },
                                    properties: { key: 'filename', bucket: 'bucket' },
                                    validation: { mimeTypes: ['image/png', 'application/pdf'] },
                                }),
                            ],
                        },
                        {
                            resource: { model: getModelByName('User'), client: prisma },
                            options: {
                                properties: { password: { isVisible: false } },
                            },
                            features: [
                                passwordsFeature({
                                    componentLoader,
                                    properties: {
                                        encryptedPassword: 'password',
                                        password: 'newPassword',
                                    },
                                    hash: async (pass) => hash(pass, 13),
                                }),
                            ],
                        },
                    ],
                },
                auth: {
                    authenticate,
                    cookieName: 'adminjs',
                    cookiePassword: 'secret',
                },
                sessionOptions: {
                    resave: true,
                    saveUninitialized: true,
                    secret: 'secret',
                },
            }),
        })
    },
)
