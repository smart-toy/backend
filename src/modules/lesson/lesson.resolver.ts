import { Args, Query, Resolver } from '@nestjs/graphql'
import { LessonService } from './lesson.service'
import { Lesson } from '@generated/lesson/lesson.model'


@Resolver(() => Lesson)
export class LessonResolver {
    constructor(
        private readonly lessonService: LessonService,
    ) {
    }

    @Query(() => [Lesson])
    async lessons(): Promise<Lesson[]> {
        return this.lessonService.findAll()
    }

    @Query(() => Lesson)
    async lesson(@Args('id', { type: () => String }) id: string): Promise<Lesson> {
        return this.lessonService.findOne(id)
    }
}
