import { Injectable } from '@nestjs/common'
import { PrismaService } from '@/prisma/prisma.service'
import { Lesson } from '@generated/lesson/lesson.model'


@Injectable()
export class LessonService {
    constructor(
        private readonly prisma: PrismaService,
    ) {
    }

    async findAll(): Promise<Lesson[]> {
        return this.prisma.lesson.findMany()
    }

    async findOne(id: string): Promise<Lesson> {
        return this.prisma.lesson.findUniqueOrThrow({ where: { id } })
    }
}
