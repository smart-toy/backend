import { Injectable } from '@nestjs/common'
import { User } from '@generated/user/user.model'
import { PrismaService } from '@/prisma/prisma.service'


@Injectable()
export class UserService {
    constructor(
        private readonly prisma: PrismaService,
    ) {
    }

    async findAll(): Promise<User[]> {
        return this.prisma.user.findMany()
    }

    async findOne(id: string): Promise<User> {
        return this.prisma.user.findUniqueOrThrow({ where: { id } })
    }
}
