import { Args, Mutation, Query, Resolver } from '@nestjs/graphql'
import { UseGuards } from '@nestjs/common'
import { GqlAuthGuard } from '../auth/gql-auth.guard'
import { UserService } from './user.service'
import { PrismaService } from '@/prisma/prisma.service'
import { AuthUser } from '@/modules/core/decorators/auth-user.decorator'
import { User } from '@generated/user/user.model'
import { CreateOneUserArgs } from '@generated/user/create-one-user.args'


@Resolver(() => User)
@UseGuards(GqlAuthGuard)
export class UserResolver {
    constructor(
        private readonly userService: UserService,
        private readonly prisma: PrismaService,
    ) {
    }

    @Query(() => User)
    async me(@AuthUser() user: User): Promise<User> {
        return user
    }

    @Query(() => [User])
    async users(): Promise<User[]> {
        return this.userService.findAll()
    }

    @Query(() => User)
    async user(@Args('id', { type: () => String }) id: string): Promise<User> {
        return this.userService.findOne(id)
    }

    @UseGuards(GqlAuthGuard)
    @Mutation(() => User)
    async createUser(@Args() args: CreateOneUserArgs): Promise<User> {
        // @ts-expect-error
        return this.prisma.user.create(args)
    }
}
