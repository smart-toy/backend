import { GraphqlConfig } from '@/config/config.interface'
import { ConfigService } from '@nestjs/config'
import { ApolloDriverConfig } from '@nestjs/apollo'
import { Injectable } from '@nestjs/common'
import { GqlOptionsFactory } from '@nestjs/graphql'
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default'


@Injectable()
export class GqlConfigService implements GqlOptionsFactory {
    constructor(private readonly configService: ConfigService) {
    }

    createGqlOptions(): ApolloDriverConfig {
        const graphqlConfig = this.configService.get<GraphqlConfig>('graphql')
        return {
            // schema options
            autoSchemaFile: graphqlConfig?.schemaDestination ?? './src/schema.graphql',
            sortSchema: graphqlConfig?.sortSchema,
            buildSchemaOptions: {
                numberScalarMode: 'integer',
            },
            // subscription
            installSubscriptionHandlers: true,
            includeStacktraceInErrorResponses: graphqlConfig?.debug,
            playground: false,
            plugins: graphqlConfig?.sandboxEnabled ? [ApolloServerPluginLandingPageLocalDefault()] : [],
            context: ({ req }) => ({ req }),
        }
    }
}
