/**
 * @desc Sort object by key
 */
export function sortObject<T>(o: Record<string, T>): Record<string, T> {
    return Object.keys(o).sort().reduce<Record<string, T>>((r, k) => {
        r[k] = o[k]!
        return r
    }, {})
}

/**
 * @desc Only one attribute of object is defined
 */
export function checkOnlyOneAttr(o: Record<string, any>): boolean {
    let counter = 0

    for (const key in o) {
        if (o[key] !== undefined) {
            counter++
            if (counter > 1) {
                return false
            }
        }
    }

    return counter == 1
}
