import { ApiProperty } from '@nestjs/swagger'


export class ForbiddenException {
    @ApiProperty()
    statusCode: number
    @ApiProperty()
    message: string
    @ApiProperty()
    error: string
}


export class UnauthorizedException {
    @ApiProperty()
    statusCode: number
    @ApiProperty()
    message: string
}
