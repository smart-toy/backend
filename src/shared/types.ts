export type AtLeast<O extends object, K extends keyof O = keyof O> = O extends unknown
    ? | (K extends keyof O ? { [P in K]: O[P] } & O : O)
    | { [P in keyof O as P extends K ? K : never]-?: O[P] } & O
    : never;
